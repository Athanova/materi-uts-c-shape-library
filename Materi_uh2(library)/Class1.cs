﻿using System;

namespace Materi_uh2_library_
{
    public class Class1
    {
        public void list_buku()
        {
            Console.WriteLine();
            Console.WriteLine("List buku");
            Console.WriteLine("Novel :");
            Console.WriteLine("1. Novel Pulang");
            Console.WriteLine("2. Novel Pergi");
            Console.WriteLine("3. Novel Hujan");
            Console.WriteLine("4. Novel Bumi");
            Console.WriteLine("5. Novel Matahari");
            Console.WriteLine("6. Novel Bintang");
            Console.WriteLine("7. Novel Komet Minor");
            Console.WriteLine("8. Novel Sebuah Seni Untuk Bodo amat");
            Console.WriteLine("9. Novel Si Anak Badai");
            Console.WriteLine("10. Novel Si Anak Cahaya");
            Console.WriteLine();
            Console.WriteLine("Komik");
            Console.WriteLine("1. Komik Doraemon");
            Console.WriteLine("2. Komik Shinchan");
            Console.WriteLine("3. Komik Si Juki");
            Console.WriteLine("4. Komik Naruto");
            Console.WriteLine("5. Komik Detektif Conan");
            Console.WriteLine("6. Komik Bleach");
            Console.WriteLine("7. Komik Kungfu Boy");
            Console.WriteLine("8. Komik Miiko");
            Console.WriteLine("9. Komik Gundala");
            Console.WriteLine("10. Komik Spider-man");
            Console.WriteLine();
            Console.WriteLine("Pengetahuan");
            Console.WriteLine("1. Matematika itu asik");
            Console.WriteLine("2. Mari berpantun");
            Console.WriteLine("3. Cara menjadi programmer");
            Console.WriteLine("4. Belajar C#");
            Console.WriteLine("5. Menjadi Hacker dalam 10 menit");
            Console.WriteLine("6. RPUL");
            Console.WriteLine("7. RPAL");
            Console.WriteLine("8. KBBI");
            Console.WriteLine("9. UUD Lengkap");
            Console.WriteLine("10. Sejarah dunia");
        }
    }
}
